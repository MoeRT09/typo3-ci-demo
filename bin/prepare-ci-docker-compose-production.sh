#!/bin/sh
cat << EOF
version: '3.2'
services:
  #######################################
  # PHP application Docker container
  #######################################
  app:
    image: $CI_REGISTRY_IMAGE/app:$CI_COMMIT_SHA
    build:
      context: .
      cache_from: 
        - $CI_REGISTRY_IMAGE/app:latest
      dockerfile: Dockerfile.cloud
    links:
      - mysql
      #- postgres
      #- mail
      #- solr
      #- elasticsearch
      #- redis
      #- memcached
      #- ftp
    ports:
      - "8000:80"
      - "8443:443"
      - "10022:22"
    restart: always
    volumes:
      - fileadmin:/app/public/fileadmin
      - typo3conf:/app/public/typo3conf
      - uploads:/app/public/uploads
    env_file:
      - etc/environment.yml
      - etc/environment.production.yml

  #######################################
  # MySQL server
  #######################################
  mysql:
    image: $CI_REGISTRY_IMAGE/mysql:$CI_COMMIT_SHA
    build:
      context: docker/mysql/
      cache_from: 
        - $CI_REGISTRY_IMAGE/mysql:latest
      #dockerfile: MySQL-5.5.Dockerfile
      # dockerfile: MySQL-5.6.Dockerfile
      #dockerfile: MySQL-5.7.Dockerfile
      #dockerfile: MariaDB-5.5.Dockerfile
      dockerfile: MariaDB-10.Dockerfile
      #dockerfile: Percona-5.5.Dockerfile
      #dockerfile: Percona-5.6.Dockerfile
      #dockerfile: Percona-5.7.Dockerfile
    restart: always
    volumes:
      - mysql:/var/lib/mysql
    env_file:
      - etc/environment.yml
      - etc/environment.production.yml

volumes:
  mysql:
  fileadmin:
  typo3conf:
  uploads:

EOF
